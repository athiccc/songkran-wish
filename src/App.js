import React, { useState, useEffect } from 'react'
import bg from './bg.jpg'
import {
  listRoom,
  createRoom,
  listMsgByRoomId,
  createMsg,
} from './controller.js'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  // useRouteMatch,
  useParams,
} from 'react-router-dom'

import './App.css'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/room/:id">
          <Room />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  )
}

function Home() {
  const [isShow, setIsShow] = useState(false)
  const [room, setRoom] = useState([])
  const [roomName, setRoomName] = useState('')

  const list = async () => {
    const list = await listRoom()
    setRoom(list)
  }

  const onClickCreateRoom = async () => {
    setIsShow(false)
    await createRoom({
      roomName: roomName,
    })

    list()
  }

  useEffect(() => {
    list()
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={bg} className="App-logo" alt="bg" />
        <button className="btn-join" onClick={() => setIsShow(true)}>
          สร้างห้อง
        </button>

        {room.map((r) => (
          <div className="card" key={r._id}>
            <p>{r.roomName}</p>
            <a className="btn-join" href={`/room/${r._id}`}>
              ร่วมอวยพร
            </a>
          </div>
        ))}

        {isShow ? (
          <div className="modal-room">
            <div className="close" onClick={() => setIsShow(false)}>
              x
            </div>
            <input
              className="input-create-room"
              placeholder="ชื่อห้อง"
              type="text"
              onChange={(e) => {
                setRoomName(e.target.value)
              }}
            />
            <button className="btn-create-room" onClick={onClickCreateRoom}>
              สร้าง
            </button>
          </div>
        ) : null}
      </header>
    </div>
  )
}

function Room(props) {
  const { id } = useParams();
  const [isShow, setIsShow] = useState(false)
  const [listmsg, setListmsg] = useState([])
  const [msg, setMsg] = useState("")
  const [from, setFrom] = useState("")

  const listMsg = async() => {
    const listmsg = await listMsgByRoomId(id)
    setListmsg(listmsg)
  }

  const onClickCreateMsg = async() => {
    setIsShow(false)
    await createMsg({
      roomId: id,
      msg: msg,
      from: from
    })

    listMsg()
  }

  useEffect(()=>{
    listMsg()
  })
  return (
    <div className="App">
      <header className="App-header room">
        {/* <img src={bg} className="App-logo" alt="bg" /> */}
        <div className="header-room"></div>
        <button className="btn-mt " onClick={() => setIsShow(true)}>
          สร้างคำอวยพร
        </button>

        {
          listmsg.map(lm =>
            <div className="wrep-card-room">
              <div className="card-room">
                <div className="detail-card">
                  {lm.msg}
                </div>
                <p className="text-from">{`จาก ${lm.from}`}</p>
              </div>  
            </div>
          )
        }

        {isShow ? (
          <div className="modal-room">
            <div className="close" onClick={() => setIsShow(false)}>
              x
            </div>
            <input placeholder="จาก" type="text" className="input-room" onChange={(e) => {setFrom(e.target.value)}}/>
            <textarea
              className="text-area-room"
              placeholder="คำอวยพร"
              type="text"
              onChange={(e) => {
                setMsg(e.target.value)
              }}
            />

            <button className="btn-room" onClick={onClickCreateMsg}>อวยพร</button>
          </div>
        ) : null}
      </header>
    </div>
  )
}

export default App
