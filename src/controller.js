import axios from 'axios'

const listRoom = async() => {
    const room = await axios.get('http://34.87.173.205:5555/room')
    return room.data
}

const createRoom = async(data) => {
    const room = await axios.post('http://34.87.173.205:5555/room', data)
    return room.data
}

const listMsgByRoomId = async(id) => {
    const msgRoom = await axios.get(`http://34.87.173.205:5555/message/${id}`)
    return msgRoom.data
}

const createMsg = async(data) => {
    const msgRoom = await axios.post(`http://34.87.173.205:5555/message`, data)
    return msgRoom.data
}

export {
    listRoom,
    createRoom,
    listMsgByRoomId,
    createMsg,
}